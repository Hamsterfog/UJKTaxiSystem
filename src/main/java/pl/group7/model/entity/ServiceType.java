package pl.group7.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by allst on 04.05.2017.
 */
@Data
@Entity
@Table(name = "SERVICE_TYPE")
@AllArgsConstructor
@NoArgsConstructor
public class ServiceType implements Persistable {

    @Id
    @GeneratedValue(generator = "service_sequence")
    @GenericGenerator(name = "service_sequence", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {@Parameter(name = "sequence_name", value = "Service_SEQ"),
                    @Parameter(name = "optimizer", value = "hilo"),
                    @Parameter(name = "initial_value", value = "1"),
                    @Parameter(name = "increment_size", value = "1")})
    @Column(name = "Service_ID")
    private long id;

    @Size(min = 3, max = 20)
    @Column(name = "NAME")
    private String name;

    @NotNull
    @Column(name = "PRICE_RATE")
    private long priceRate;

}