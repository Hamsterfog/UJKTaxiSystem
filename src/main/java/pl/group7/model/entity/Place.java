package pl.group7.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by allst on 24.05.2017.
 */
@Data
@Entity
@Table(name = "PLACE")
@AllArgsConstructor
@NoArgsConstructor
public class Place implements Persistable {

    @Id
    @GeneratedValue(generator = "place_sequence")
    @GenericGenerator(name = "place_sequence", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {@Parameter(name = "sequence_name", value = "Place_SEQ"),
                    @Parameter(name = "optimizer", value = "hilo"),
                    @Parameter(name = "initial_value", value = "1"),
                    @Parameter(name = "increment_size", value = "1")})
    @Column(name = "Place_ID")
    private long id;

    @Size(min = 3, max = 20)
    @Column(name = "CITY")
    private String city;

    @Size(min = 3, max = 20)
    @Column(name = "STREET")
    private String street;

    @Size(min = 6, max = 6)
    @Column(name = "POSTAL_CODE")
    private String postalCode;

    @NotNull
    @Column(name = "HOME_NUMBER")
    private int homeNumber;

    @Column(name = "APARTMENT_NUMBER")
    private int apartmentNumber;

}
