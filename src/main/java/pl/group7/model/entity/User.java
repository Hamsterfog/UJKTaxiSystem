package pl.group7.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import pl.group7.model.data.Role;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by allst on 01.03.2017.
 */
@Data
@Entity
@Table(name = "Usr")
@AllArgsConstructor
@NoArgsConstructor
public class User implements Persistable {

    @Id
    @GeneratedValue(generator = "usr_sequence")
    @GenericGenerator(name = "usr_sequence", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
                parameters = {@Parameter(name = "sequence_name", value = "Usr_SEQ"),
                              @Parameter(name = "optimizer", value = "hilo"),
                              @Parameter(name = "initial_value", value = "1"),
                              @Parameter(name = "increment_size", value = "1")})
    @Column(name = "USER_ID")
    private long id;

    @Size(min = 3, max = 20)
    @Column(name = "NAME")
    private String name;

    @Size(min = 3, max = 20)
    @Column(name = "SURNAME")
    private String surname;

    @Size(min = 3, max = 20)
    @Column(name = "LOGIN")
    private String login;

    @Size(min = 3, max = 20)
    @Column(name = "PASSWORD")
    private String password;

    @Size(min = 5, max = 35)
    @Column(name = "MAIL_ADDRESS")
    private String mailAddress;

    @NotNull
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "JOIN_DATE")
    private Date joinDate;

    @NotNull
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "LAST_LOGIN_DATE")
    private Date lastLoginDate;

    @NotNull
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "BIRTHDAY_DATE")
    private Date birthDay;

    @NotNull
    @Size(min = 8, max = 20)
    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(name = "ROLES")
    @Enumerated(EnumType.STRING)
    @ElementCollection(targetClass = Role.class)
    private List<Role> role;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer")
    private List<Order> ordersMade = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "consultant")
    private List<Order> ordersDispatched = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "driver")
    private List<Order> ordersAssigned = new ArrayList<>();


}
