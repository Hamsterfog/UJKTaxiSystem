package pl.group7.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import pl.group7.model.data.State;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by allst on 04.05.2017.
 */
@Data
@Entity
@Table(name = "SERVICE_ORDER")
@AllArgsConstructor
@NoArgsConstructor
public class Order implements Persistable {

    @Id
    @GeneratedValue(generator = "order_sequence")
    @GenericGenerator(name = "order_sequence", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator",
            parameters = {@Parameter(name = "sequence_name", value = "Order_SEQ"),
                    @Parameter(name = "optimizer", value = "hilo"),
                    @Parameter(name = "initial_value", value = "1"),
                    @Parameter(name = "increment_size", value = "1")})
    @Column(name = "ORDER_ID")
    private long id;

    @NotNull
    @JoinColumn(name = "CUSTOMER_ID")
    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.PERSIST)
    private User customer;

    @JoinColumn(name = "CONSULTANT_ID")
    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.PERSIST)
    private User consultant;

    @JoinColumn(name = "DRIVER_ID")
    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.PERSIST)
    private User driver;

    @NotNull
    @JoinColumn(name = "SOURCE_ID")
    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Place source;

    @NotNull
    @JoinColumn(name = "DESTINATION_ID")
    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Place destination;

    @NotNull
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    @Column(name = "ORDER_DATE")
    private Date orderDate;

    @Column(name = "DISTANCE")
    private double distance;

    @Column(name = "STATE")
    @Enumerated(EnumType.STRING)
    private State state;

    @NotNull
    @JoinColumn(name = "SERVICE_TYPE_ID")
    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private ServiceType serviceType;



}
