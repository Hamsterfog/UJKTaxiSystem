package pl.group7.model.data;

/**
 * Created by wojciechowskid on 05/05/2017.
 */
public enum Permission {
    SU,MAKE_ORDER,RECEIVE_ORDER,DISTPATCH_ORDER,
    VIEW_ALL_ORDERS,VIEW_DISPATCHED_ORDERS,
    VIEW_NON_DISPATCHED_ORDERS,MODIFY_ORDERS
}
