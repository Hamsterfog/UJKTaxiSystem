package pl.group7.model.data;

/**
 * Created by allst on 21.03.2017.
 */
public enum Role {

    ADMIN(new Permission[]{Permission.SU}),
    USER(new Permission[]{Permission.MAKE_ORDER}),
    DBADMIN(new Permission[]{Permission.SU}),
    CONSULTANT(new Permission[]{Permission.DISTPATCH_ORDER,Permission.VIEW_NON_DISPATCHED_ORDERS}),
    DRIVER(new Permission[]{Permission.RECEIVE_ORDER});

    private Permission[] permissions;

    Role(Permission[] permissions) {
        this.permissions = permissions;
    }

    public Permission[] getPermissions(){
        return this.permissions;
    }

}
