package pl.group7.service.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.group7.model.data.State;
import pl.group7.model.entity.Order;
import pl.group7.model.entity.User;
import pl.group7.repository.OrderRepository;
import pl.group7.service.messaging.MailContentHelper;
import pl.group7.utility.CompareUtility;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * Created by allst on 04.05.2017.
 */
@Service
public class OrderService {

    private final Logger logger = LoggerFactory.getLogger(OrderService.class);

    private OrderRepository orderRepository;
    private MailContentHelper contentHelper;

    @Autowired
    public OrderService(OrderRepository orderRepository, MailContentHelper contentHelper) {
        this.orderRepository = orderRepository;
        this.contentHelper = contentHelper;
    }

    public Order save(Order order) {
        if (orderRepository.findOne(order.getId()) == null) {
            try {
                order = orderRepository.save(order);
                //                Map<String,String> valuesMap = contentHelper.getDataForOrderMadeMail(order);
                //                contentHelper.sendOrderMade(valuesMap);
            } catch (Exception e) {
                logger.debug(e.getMessage(), e);
                throw e;
            }
            return order;
        } else {
            EntityExistsException e = new EntityExistsException("This order already exists in db, find another");
            logger.info(e.getMessage(), e);
            throw e;
        }
    }

    public Order getById(long id) {
        return orderRepository.findOne(id);
        //TODO LOGGING LOGIC
        //TODO HANDLING EXCEPTIONS
    }

    public boolean removeById(long id) {
        if (orderRepository.findOne(id) != null) {
            try {
                orderRepository.removeById(id);
                return true;
            } catch (Exception e) {
                logger.debug(e.getMessage(), e);
                throw e;
            }
        } else {
            EntityNotFoundException e = new EntityNotFoundException("There is no such order in DB.");
            logger.info(e.getMessage(), e);
            throw e;
        }
    }

    public Order modify(Order order) {
        if (orderRepository.findOne(order.getId()) != null) {
            try {
                return orderRepository.save(order);
            } catch (Exception e) {
                logger.debug(e.getMessage(), e);
                throw e;
            }
        } else {
            EntityNotFoundException e = new EntityNotFoundException("There is no such order in DB.");
            logger.info(e.getMessage(), e);
            throw e;
        }
    }

    public List<Order> findAllOrdersForUser(User customer) {
        return orderRepository.findOrdersByCustomerOrderByOrderDateDesc(customer);
        //TODO LOGGING LOGIC
        //TODO HANDLING EXCEPTIONS
    }

    public List<Order> findNonAssignedOrders() {
        return orderRepository.findAllByDriverIsNull();
    }

    public Order assignToDriver(Order order) {
        Order baseOrder = orderRepository.findOne(order.getId());
        if (baseOrder != null) {
            try {
                if (CompareUtility.isAssignable(baseOrder, order)) {
                    return orderRepository.save(order);
                } else {
                    throw new IllegalArgumentException("Illegal order, verify it");
                }
            } catch (Exception e) {
                logger.debug(e.getMessage(), e);
                throw e;
            }
        } else {
            EntityNotFoundException e = new EntityNotFoundException("There is no such order in DB.");
            logger.info(e.getMessage(), e);
            throw e;
        }
    }

    public Order completeOrder(Order order) {
        Order baseOrder = orderRepository.findOne(order.getId());
        if (baseOrder != null) {
            try {
                if (CompareUtility.isFinishable(baseOrder, order) && (order.getDistance() != 0d)) {
                    return orderRepository.save(order);
                } else {
                    throw new IllegalArgumentException("Illegal order, verify it");
                }
            } catch (Exception e) {
                logger.debug(e.getMessage(), e);
                throw e;
            }
        } else {
            EntityNotFoundException e = new EntityNotFoundException("There is no such order in DB.");
            logger.info(e.getMessage(), e);
            throw e;
        }
    }

    public List<Order> findAllOrdersForDriver(User user) {
        return orderRepository.findAllByDriver(user);
    }

    public List<Order> findNonFinishedOrders(User user){
        return orderRepository.findAllByDriverAndState(user, State.IN_PROCESS);
    }
}
