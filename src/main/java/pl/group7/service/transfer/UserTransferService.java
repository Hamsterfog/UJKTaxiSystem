package pl.group7.service.transfer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.group7.model.entity.Order;
import pl.group7.model.entity.User;
import pl.group7.model.to.UserTO;
import pl.group7.service.data.OrderService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by wojciechowskid on 08/05/2017.
 */
@Service
public class UserTransferService implements TransferService<User, UserTO> {

    @Autowired
    OrderService orderService;

    @Override
    public User getObject(UserTO to) {
        User user = new User();
        user.setId(to.getId());
        user.setName(to.getName());
        user.setSurname(to.getSurname());
        user.setLogin(to.getLogin());
        user.setPassword(to.getPassword());
        user.setBirthDay(to.getBirthDay());
        user.setJoinDate(to.getJoinDate());
        user.setLastLoginDate(to.getLastLoginDate());
        user.setMailAddress(to.getMailAddress());
        user.setPhoneNumber(to.getPhoneNumber());
        user.setRole(to.getRoleList());
        if (to.getOrdersMade() != null) {
            List<Order> orders = to.getOrdersMade().stream().map(orderId -> orderService.getById(orderId))
                    .collect(Collectors.toList());
            user.setOrdersMade(orders);
        }
        if (to.getOrdersDispatched() != null) {
            List<Order> orders = to.getOrdersDispatched().stream().map(orderId -> orderService.getById(orderId))
                    .collect(Collectors.toList());
            user.setOrdersDispatched(orders);
        }
        if (to.getOrdersDispatched() != null) {
            List<Order> orders = to.getOrdersDispatched().stream().map(orderId -> orderService.getById(orderId))
                    .collect(Collectors.toList());
            user.setOrdersDispatched(orders);
        }

        return user;
    }

    @Override
    public UserTO getTO(User persist) {
        UserTO userTo = new UserTO();
        userTo.setId(persist.getId());
        userTo.setName(persist.getName());
        userTo.setSurname(persist.getSurname());
        userTo.setLogin(persist.getLogin());
        userTo.setPassword(persist.getPassword());
        userTo.setBirthDay(persist.getBirthDay());
        userTo.setJoinDate(persist.getJoinDate());
        userTo.setLastLoginDate(persist.getLastLoginDate());
        userTo.setMailAddress(persist.getMailAddress());
        userTo.setPhoneNumber(persist.getPhoneNumber());
        userTo.setRoleList(persist.getRole());
        userTo.setOrdersMade(persist.getOrdersMade().stream().map(order -> order.getId()).collect(Collectors.toList()));
        userTo.setOrdersDispatched(persist.getOrdersDispatched().stream().map(order -> order.getId()).collect(Collectors.toList()));
        userTo.setOrdersAssigned(persist.getOrdersAssigned().stream().map(order -> order.getId()).collect(Collectors.toList()));
        return userTo;
    }
}
