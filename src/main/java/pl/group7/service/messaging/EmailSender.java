package pl.group7.service.messaging;

/**
 * Created by allst on 18.05.2017.
 */
public interface EmailSender {
    void sendEmail(String to, String subject, String content);
}