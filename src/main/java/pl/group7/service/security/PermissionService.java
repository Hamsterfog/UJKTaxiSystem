package pl.group7.service.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;
import pl.group7.model.data.Permission;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by allst on 05.05.2017.
 */
@Service
public class PermissionService {

    public boolean hasPermission(UsernamePasswordAuthenticationToken principal, Permission... permission){
        Set<String> permissions = Arrays.stream(permission).map(p -> p.name()).collect(Collectors.toSet());
        return principal
                .getAuthorities()
                .stream()
                .filter(p -> permissions.contains(p.getAuthority()))
                .findAny()
                .isPresent();
    }
}
