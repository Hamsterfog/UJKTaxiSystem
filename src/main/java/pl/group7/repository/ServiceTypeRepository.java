package pl.group7.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.group7.model.entity.ServiceType;

import java.util.List;

/**
 * Created by allst on 27.05.2017.
 */
public interface ServiceTypeRepository extends JpaRepository<ServiceType, Long> {
    List<ServiceType> findAll();
    ServiceType getServiceTypeById(Long id);
}
