package pl.group7.repository;

/**
 * Created by allst on 01.03.2017.
 */

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import pl.group7.model.data.Role;
import pl.group7.model.entity.User;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByLogin(String login);
    List<User> findAllByRoleContains(Role role);
    @Transactional
    void removeById(long id);

}
