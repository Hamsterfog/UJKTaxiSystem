package pl.group7.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfFilter;
import pl.group7.model.data.Permission;
import pl.group7.security.CsrfTokenResponseHeaderBindingFilter;
import pl.group7.security.RESTAuthenticationFailureHandler;
import pl.group7.security.RESTAuthenticationSuccessHandler;
import pl.group7.service.security.CustomUserDetailsService;

/**
 * created by : Dominik 12.03.2017
 * */
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    CustomUserDetailsService userDetailsService;
    @Autowired
    private RESTAuthenticationFailureHandler authenticationFailureHandler;
    @Autowired
    private RESTAuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/admin/**").hasAuthority(Permission.SU.name())
                .antMatchers(HttpMethod.GET,"/api/user/login/*").permitAll()
                .antMatchers(HttpMethod.GET,"/api/user").hasAuthority(Permission.SU.name())
                .antMatchers(HttpMethod.GET,"/api/user/availableDrivers").hasAuthority(Permission.VIEW_NON_DISPATCHED_ORDERS.name())
                .antMatchers(HttpMethod.GET,"/api/order/all").authenticated()
                .antMatchers(HttpMethod.GET,"/api/order/nonAssigned").hasAuthority(Permission.VIEW_NON_DISPATCHED_ORDERS.name())
                .antMatchers(HttpMethod.GET,"/api/order/getAssignedOrders").hasAuthority(Permission.RECEIVE_ORDER.name())
                .antMatchers(HttpMethod.GET,"/api/service/available").authenticated()
                .antMatchers(HttpMethod.GET,"/api/user/^[0-9]*$","/api/order/^[0-9]*$").hasAuthority(Permission.SU.name())
                .antMatchers(HttpMethod.POST,"/api/order/assignToDriver").hasAuthority(Permission.DISTPATCH_ORDER.name())
                .antMatchers(HttpMethod.POST,"/api/order/finishOrder").hasAuthority(Permission.RECEIVE_ORDER.name())
                .antMatchers(HttpMethod.POST,"/api/user").permitAll()
                .antMatchers(HttpMethod.POST,"/api/user/login").authenticated()
                .antMatchers(HttpMethod.POST,"/api/user/add").hasAuthority(Permission.SU.name())
                .antMatchers(HttpMethod.POST, "/api/order").hasAuthority(Permission.MAKE_ORDER.name())
                .antMatchers(HttpMethod.PUT, "/api/user","/api/order").authenticated()
                .antMatchers(HttpMethod.DELETE,"api/user/^[0-9]*$").authenticated()
                .antMatchers(HttpMethod.DELETE, "/api/order/^[0-9]*$").hasAuthority(Permission.SU.name())
                .and().formLogin().successHandler(authenticationSuccessHandler)
                .and().formLogin().failureHandler(authenticationFailureHandler);
        http.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                .and().addFilterAfter(new CsrfTokenResponseHeaderBindingFilter(), CsrfFilter.class);
//        http.csrf().disable();
    }

}