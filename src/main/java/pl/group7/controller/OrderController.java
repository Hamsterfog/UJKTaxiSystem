package pl.group7.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import pl.group7.model.data.Permission;
import pl.group7.model.data.State;
import pl.group7.model.entity.Order;
import pl.group7.model.entity.User;
import pl.group7.model.to.OrderTO;
import pl.group7.service.data.OrderService;
import pl.group7.service.data.UserService;
import pl.group7.service.security.PermissionService;
import pl.group7.service.transfer.OrderTransferService;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by allst on 04.05.2017.
 */
@RestController
@RequestMapping(value = "/order")
public class OrderController {

    private OrderService orderService;
    private UserService userService;
    private PermissionService permissionService;
    private OrderTransferService orderTransferService;

    @Autowired
    public OrderController(OrderService orderService, UserService userService, PermissionService permissionService,
                           OrderTransferService orderTransferService) {
        this.orderService = orderService;
        this.userService = userService;
        this.permissionService = permissionService;
        this.orderTransferService = orderTransferService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Order getOrderById(@PathVariable("id") Long id) {
        return orderService.getById(id);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public OrderTO addOrder(@RequestBody OrderTO orderTO, UsernamePasswordAuthenticationToken principal) {
        Order order = orderTransferService.getObject(orderTO);
        order.setOrderDate(new Date());
        order.setCustomer(userService.findByLogin(principal.getName()));
        order.setState(State.OPEN);
        OrderTO to = orderTransferService.getTO(orderService.save(order));
        return to;
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public OrderTO modifyOrder(@RequestBody OrderTO orderTO, UsernamePasswordAuthenticationToken principal) throws  IllegalAccessException{
        Order order = orderTransferService.getObject(orderTO);
        if (principal.getName().equals(order.getCustomer().getLogin()) ||
                permissionService.hasPermission(principal, Permission.SU,Permission.MODIFY_ORDERS)){
            return orderTransferService.getTO(orderService.modify(order));
        } else {
            throw new IllegalAccessException("You are not allowed to modify order");
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void removeOrder(@PathVariable("id") Long id) {
        orderService.removeById(id);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<OrderTO> getAllOrdersForLoggedUser(UsernamePasswordAuthenticationToken principal) throws  IllegalAccessException{
        User user = userService.findByLogin(principal.getName());
        List<Order> orders = orderService.findAllOrdersForUser(user);
        return orders.stream().map(o -> orderTransferService.getTO(o)).collect(Collectors.toList());
    }

    @RequestMapping(value = "/nonAssigned", method =  RequestMethod.GET)
    public List<OrderTO> findAllNonAssignedOrders(){
        List<Order> orders = orderService.findNonAssignedOrders();
        return orders.stream().map(o -> orderTransferService.getTO(o)).collect(Collectors.toList());
    }

    @RequestMapping(value = "/assignToDriver", method =  RequestMethod.POST)
    public OrderTO assignToDriver(@RequestBody OrderTO orderTO, UsernamePasswordAuthenticationToken principal ){
        Order order = orderTransferService.getObject(orderTO);
        order.setConsultant(userService.findByLogin(principal.getName()));
        order.setState(State.IN_PROCESS);
        return orderTransferService.getTO(orderService.assignToDriver(order));
    }

    @RequestMapping(value = "/finishOrder", method =  RequestMethod.POST)
    public OrderTO completeOrder(@RequestBody OrderTO orderTO){
        Order order = orderTransferService.getObject(orderTO);
        order.setState(State.FINISHED);
        return orderTransferService.getTO(orderService.completeOrder(order));
    }

    @RequestMapping(value = "/getAssignedOrders", method = RequestMethod.GET)
    public List<OrderTO> getAssignedOrders(UsernamePasswordAuthenticationToken principal){
        List<Order> orders = orderService.findNonFinishedOrders(userService.findByLogin(principal.getName()));
        return orders.stream().map(o -> orderTransferService.getTO(o)).collect(Collectors.toList());
    }

}
