package pl.group7.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import pl.group7.model.data.Permission;
import pl.group7.model.entity.User;
import pl.group7.model.to.UserTO;
import pl.group7.service.data.UserService;
import pl.group7.service.security.PermissionService;
import pl.group7.service.transfer.UserTransferService;
import pl.group7.utility.TempFilling;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by allst on 01.03.2017.
 */
@RestController
@RequestMapping(value = "/user")
public class UserController {

    private UserService userService;
    private TempFilling filling;
    private PermissionService permissionService;
    private UserTransferService userTransferService;

    @Autowired
    public UserController(UserService userService, TempFilling filling, PermissionService permissionService, UserTransferService userTransferService) {
        this.userService = userService;
        this.filling = filling;
        this.permissionService = permissionService;
        this.userTransferService = userTransferService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public UserTO getUserById(@PathVariable("id") Long id) {
        return userTransferService.getTO(userService.getById(id));
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public UserTO getUserByLogin(UsernamePasswordAuthenticationToken principal) {
        return userTransferService.getTO(userService.findByLogin(principal.getName()));
    }

    @RequestMapping(value = "/login/{login}", method = RequestMethod.GET)
    public void checkIfLoginIsAvailable(@PathVariable("login") String login) {
        if (userService.findByLogin(login) != null) {
            throw new EntityExistsException("Login is already taken");
        }
    }


    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserTO addUser(@RequestBody UserTO userTO) {
        User user = userTransferService.getObject(userTO);
        user.setJoinDate(new Date());
        user.setLastLoginDate(new Date());
        return userTransferService.getTO(userService.save(user));
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public UserTO modifyUser(@RequestBody UserTO userTO, UsernamePasswordAuthenticationToken principal) throws IllegalAccessException {
        User user = userTransferService.getObject(userTO);
        if (principal.getName().equals(userTO.getLogin()) || permissionService
                .hasPermission(principal, Permission.SU)) {
            return userTransferService.getTO(userService.modify(user));
        } else {
            throw new IllegalAccessException("You are not allowed to modify user");
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<UserTO> getAllUsers() {
        List<UserTO> userTOS = userService.findAll().stream().map(u -> userTransferService.getTO(u))
                .collect(Collectors.toList());
        return userTOS;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void removeUser(@PathVariable("id") Long id, UsernamePasswordAuthenticationToken principal) throws IllegalAccessException {
        User targetUser = userService.getById(id);
        if (targetUser != null && (principal.getName().equals(targetUser.getLogin()) || permissionService
                .hasPermission(principal, Permission.SU))) {
            userService.removeById(id);
        } else if (targetUser == null) {
            throw new EntityNotFoundException("There is no such user in DB.");
        } else {
            throw new IllegalAccessException("You are not allowed to remove user");
        }
    }

    @RequestMapping(value = "/availableDrivers", method = RequestMethod.GET)
    public List<UserTO> findNonAssignedDrivers() {
        List<User> users = userService.findAllNonAssignedDrivers();
        return users.stream().map(u -> userTransferService.getTO(u)).collect(Collectors.toList());
    }

}
