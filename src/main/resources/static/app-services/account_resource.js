(function() {
    'use strict';

    angular
        .module('accountResource', [])
        .factory('accountService', accountService);

        accountService.$inject = ['$resource', 'registerSrv'];

        function accountService($resource, registerSrv ) {
            var service = {
                register : register,
                userExist : userExist
            };

            return service;

            function register(account, success, failure) {
                new registerSrv.register(account, function(){
                    success();
                },
                function(){
                    failure();
                });
            }

            function userExist(username, success, failure) {
                var checkUsername =  $resource('http://localhost:8080/api/user/login/:username');
                checkUsername.get({username: username}, function(){
                    success();
                }, function(){
                    failure();
                });

            }
        }
})();