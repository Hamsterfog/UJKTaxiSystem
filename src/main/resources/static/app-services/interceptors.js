(function() {
    'use strict';

    angular
        .module('app')
        .config(config)
        .factory('interceptor', interceptor);

        function config($httpProvider) {
            $httpProvider.interceptors.push('interceptor');
        }

        interceptor.$inject = ['tokenSrv', '$q', '$location', '$rootScope'];

        function interceptor(tokenSrv, $q, $location, $rootScope) {
            return{
                request: function(config) {
                    config.headers['Authorization'] = 'Basic';
                    config.headers['X-CSRF-HEADER'] = 'X-CSRF-TOKEN';
                    config.headers['X-CSRF-PARAM'] = '_csrf';
                    if(tokenSrv.show()) {
                        config.headers['X-CSRF-TOKEN'] = tokenSrv.show();
                    }
                    return config;
                },
                response: function(response){
                    if(response.headers('x-csrf-token')) {
                        tokenSrv.add(response.headers('x-csrf-token'));
                    }
                    return response;
                }
            };
        }
})();