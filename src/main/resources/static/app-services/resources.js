(function() {
    'use strict';

    angular
        .module('app')
        .factory('loginSrv', loginSrv)
        .factory('logoutSrv', logoutSrv)
        .factory('registerSrv', registerSrv)
        .factory('userSrv', userSrv)
        .factory('orderSrv', orderSrv);

        loginSrv.$inject = ['$resource'];
        logoutSrv.inject = ['$resource'];
        registerSrv.$inject = ['$resource'];
        userSrv.$inject = ['$resource'];
        orderSrv.$inject = ['$resource'];

        function loginSrv($resource){
            return  $resource('http://localhost:8080/login', {},
            {
                login: {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }
            });
        }

        function logoutSrv($resource){
            return $resource('http://localhost:8080/logout');
        }

        function registerSrv($resource){
            return $resource("http://localhost:8080/api/user/", {},
            {
                register: {
                    method: "POST"
                }
            });
        }

        function userSrv($resource){
            return $resource("http://localhost:8080/api/user/:id", { id: '@id' },
            {
                // Konfiguracja zapytań
                create: {
                    method: "POST"
                },
                save: {
                    method: "PUT",
                    url: "http://localhost:8080/api/user/"
                },
                // tymczasowa konfiguracja potrzebna do pobrania użytkowników
                load: {
                    method: "GET",
                    url: "http://localhost:8080/api/user/fill"
                }
            });
        }

        function orderSrv($resource){
            return $resource("http://localhost:8080/api/order/:id", {},
                {
                    create: {
                        method: "POST"
                    },
                    getServices: {
                        method: "GET",
                        url:"http://localhost:8080/api/service/available",
                        isArray: true
                    },
                    userOrders : {
                        method: "GET",
                        url: "http://localhost:8080/api/order/all",
                        isArray: true
                    },
                    consultantOrders: {
                        method: "GET",
                        url:"http://localhost:8080/api/order/nonAssigned",
                        isArray: true
                    },
                    driverOrders: {
                        method: "GET",
                        url: "http://localhost:8080/api/order/getAssignedOrders",
                        isArray: true
                    },
                    availableDrivers : {
                        method: "GET",
                        url: "http://localhost:8080/api/user/availableDrivers",
                        isArray: true
                    },
                    submitDriver: {
                        method: "POST",
                        url:  "http://localhost:8080/api/order/assignToDriver"
                    },
                    finishOrder: {
                        method: "POST",
                        url:  "http://localhost:8080/api/order/finishOrder"
                    }
                });
        }
})();