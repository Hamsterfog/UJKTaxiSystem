(function() {
    'use strict';

    angular
        .module('app')
        .config(config)
        .factory('sessionSrv', sessionSrv);

        sessionSrv.$inject = ['store', '$resource', '$state', 'loginSrv', 'logoutSrv', '$q'];

        function config(storeProvider) {
            storeProvider.setStore('sessionStorage');
        }

        function sessionSrv(store, $resource, $state, loginSrv, logoutSrv, $q) {
            var session = {
                role : {},
                login: login,
                setRole: setRole,
                isLoggedIn : isLoggedIn,
                logout: logout,
                getUser: getUser
            };

            return session;

            function login(account, success, failure) {
                loginSrv.login('username=' + account.username + '&' + 'password=' + account.password,
                    function () {
                        success();
                        store.set('session', true);

                        //var authdata = Base64.encode(account.username + ':' + account.password);
                        //store.set('session', authdata);
                    },
                    function () {
                        failure();
                    }
                );
            }

            function isLoggedIn() {
                return (store.get('session') !== null);
            }

            function setRole(role) {
                session.role = role;
            }

            function logout() {
                logoutSrv.save(function () {
                    store.remove('session');
                    session.role = {};
                    $state.go('main.home').then(function(){
                        $state.reload();
                    });

                });
            }

            function getUser() {
                var deferred = $q.defer();
                    var user = $resource('http://localhost:8080/api/user/login', {},
                        {
                            login: {
                                method: 'POST',
                                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                            }
                        });

                    user.login({}, function(res){
                        deferred.resolve(res);
                    }, function(){
                        deferred.reject('Wystąpił błąd podczas pobierania danych');
                    });
                return deferred.promise;
            }
         }

        var Base64 = {

        keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

            encode: function (input) {
                var output = "";
                var chr1, chr2, chr3 = "";
                var enc1, enc2, enc3, enc4 = "";
                var i = 0;

                do {
                    chr1 = input.charCodeAt(i++);
                    chr2 = input.charCodeAt(i++);
                    chr3 = input.charCodeAt(i++);

                    enc1 = chr1 >> 2;
                    enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                    enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                    enc4 = chr3 & 63;

                    if (isNaN(chr2)) {
                        enc3 = enc4 = 64;
                    } else if (isNaN(chr3)) {
                        enc4 = 64;
                    }

                    output = output +
                        this.keyStr.charAt(enc1) +
                        this.keyStr.charAt(enc2) +
                        this.keyStr.charAt(enc3) +
                        this.keyStr.charAt(enc4);
                    chr1 = chr2 = chr3 = "";
                    enc1 = enc2 = enc3 = enc4 = "";
                } while (i < input.length);

                return output;
            },

            decode: function (input) {
                var output = "";
                var chr1, chr2, chr3 = "";
                var enc1, enc2, enc3, enc4 = "";
                var i = 0;

                // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
                var base64test = /[^A-Za-z0-9\+\/\=]/g;
                if (base64test.exec(input)) {
                    window.alert("There were invalid base64 characters in the input text.\n" +
                        "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                        "Expect errors in decoding.");
                }
                input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

                do {
                    enc1 = this.keyStr.indexOf(input.charAt(i++));
                    enc2 = this.keyStr.indexOf(input.charAt(i++));
                    enc3 = this.keyStr.indexOf(input.charAt(i++));
                    enc4 = this.keyStr.indexOf(input.charAt(i++));

                    chr1 = (enc1 << 2) | (enc2 >> 4);
                    chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                    chr3 = ((enc3 & 3) << 6) | enc4;

                    output = output + String.fromCharCode(chr1);

                    if (enc3 != 64) {
                        output = output + String.fromCharCode(chr2);
                    }
                    if (enc4 != 64) {
                        output = output + String.fromCharCode(chr3);
                    }

                    chr1 = chr2 = chr3 = "";
                    enc1 = enc2 = enc3 = enc4 = "";

                } while (i < input.length);

                return output;
            }
        };
})();