(function() {
    'use strict';

    angular
        .module('app')
        .config(config)
        .factory('tokenSrv', tokenSrv);

        tokenSrv.$inject = ['store'];

        // konfiguracja angular-storage do wyboru sessionStorage, localSorage i coocies
        function config(storeProvider) {
            storeProvider.setStore('sessionStorage');
        }

        function tokenSrv(store) {
            var token = {
                key : "",
                add : add,
                show: show
            };

            return token;

            // funckja add zapisująca token do angular-storage
           function add(csrf) {
                token.key = csrf;
                store.set('key', csrf)
            }

            // Funkcja show zwracająca zapisany token
            function show() {
                return token.key;
            }
        }
})();