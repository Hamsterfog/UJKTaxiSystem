(function() {
    'use strict';

    angular
        .module('app')
        .controller("usersListController", usersListController);

        usersListController.$inject = ['userSrv'];

        function usersListController(userSrv){
            var vm = this;

            vm.loadUsers = loadUsers;
            vm.usersList =  usersList;
            vm.deleteUser = deleteUser;

            // Funkcja loadUser ładuje użytkowników (tymczasowa)
            function loadUsers(){
                userSrv.load(function(){
                    vm.usersList();
                })
            }

            //Funckja usersList, w której do zmiennej users zostaje przypisana tablica obiektów resource po wykonaniu zapytania query()
             function usersList() {
                vm.users = userSrv.query();
             }

            //Funckja deleteUser usuwa element z tablicy resource oraz z serwera + uaktualnia widok
            function deleteUser(user) {
                user.$delete().then( function() {
                    vm.users.splice(vm.users.indexOf(user), 1);
                });
            }
            vm.usersList();
        }
})();