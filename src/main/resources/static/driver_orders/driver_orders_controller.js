(function() {
    'use strict';

    angular
        .module("app")
        .controller("driverOrdersController", driverOrdersController);

        driverOrdersController.$inject = ['orderSrv'];

        function driverOrdersController(orderSrv) {
            var vm = this;

            vm.orders = {};
            vm.distance = 0;

            vm.setDistance = setDistance;
            vm.finish = finish;

            function setDistance(distance) {
                console.log(distance);
                vm.distance = distance;
            }

            function getOrders(){
                vm.orders = orderSrv.driverOrders();
            }

            function finish(order) {
                order.distance = vm.distance;

                console.log(order);
                orderSrv.finishOrder(order, function () {
                    getOrders();
                });
            }

            getOrders();
        }
})();