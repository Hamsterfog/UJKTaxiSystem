(function() {
    'use strict';

    angular
        .module('app', [
            'ui.router',
            'ngAnimate',
            'ngResource',
            'angular-storage',
            'ui.bootstrap',
            'accountResource',
            'directives'
        ])
        .config(config)
        .run(run);

        function config($stateProvider, $urlRouterProvider, $locationProvider) {

            //wyłaczenie html5Mode i usunięcie prefixów
            $locationProvider.html5Mode(false).hashPrefix('');

            $stateProvider
                .state('main', {
                    url: '',
                    templateUrl: 'navigation/navigation_main_view.html',
                    controller: 'navigationController',
                    controllerAs: 'vm',
                    abstract: true
                })
                // Stona główna
                .state('main.home', {
                    url: '/',
                    templateUrl: 'home/home_view.html',
                    controller: 'homeController',
                    controllerAs: 'vm',
                    data : { pageTitle : "Home" }
                })
                // Panel logowania
                .state('main.login', {
                    url: '/login',
                    templateUrl: 'account/login_view.html',
                    controller: 'loginController',
                    controllerAs: 'vm',
                    data : { pageTitle : "Login" }
                })
                // Panel rejestracji
                .state('main.register', {
                    url: '/register',
                    templateUrl: 'account/register_view.html',
                    controller: 'registerController',
                    controllerAs: 'vm',
                    data : { pageTitle : "Registration" }
                })
                .state('user', {
                    url: '',
                    templateUrl: 'navigation/navigation_user_view.html',
                    controller: 'navigationController',
                    controllerAs: 'vm',
                    abstract: true
                })
                // Edycja profilu
                .state('user.editProfile', {
                    url: '/user/edit',
                    templateUrl: 'user_edit_profile/user_edit_profile_view.html',
                    controller: 'userProfileController',
                    controllerAs: 'vm',
                    data : { pageTitle : "User profile" },
                    authorizeRoles: ['ADMIN', 'USER', 'CONSULTANT']
                })
                // Zamówienia użytkownika
                .state('user.userOrders', {
                    url: '/user/orders',
                    templateUrl: 'user_orders/user_orders_view.html',
                    controller: 'userOrdersController',
                    controllerAs: 'vm',
                    data : { pageTitle : "Orders" },
                    authorizeRoles: ['USER']
                })
                // Zamówienia konsultanta
                .state('user.consultantOrders', {
                    url: '/consultant/orders',
                    templateUrl: 'consultant_orders/consultant_orders_view.html',
                    controller: 'consultantOrdersController',
                    controllerAs: 'vm',
                    data : { pageTitle : "Orders" },
                    authorizeRoles: ['CONSULTANT']
                })
                // Zamówienia kierowcy
                .state('user.driverOrders', {
                    url: '/driver/orders',
                    templateUrl: 'driver_orders/driver_orders_view.html',
                    controller: 'driverOrdersController',
                    controllerAs: 'vm',
                    data : { pageTitle : "Orders" },
                    authorizeRoles: ['DRIVER']
                })
                // Lista użytkowników w panelu administratora
                .state('user.adminUsers', {
                    url: '/admin/users',
                    templateUrl: 'admin_users/users_list_view.html',
                    controller: 'usersListController',
                    controllerAs: 'vm',
                    data : { pageTitle : "Users" },
                    authorizeRoles: ['ADMIN']
                })
                // Edycja pojedynczego użytkownika w panelu administratora
                .state('user.adminEditUser', {
                    url: '/admin/user/edit/:id',
                    templateUrl: 'admin_users//edit_user_view.html',
                    controller: 'editUserController',
                    controllerAs: 'vm',
                    data : { pageTitle : "Edit user" },
                    authorizeRoles: ['ADMIN']
                })
                // Tworzenie nowego użytkownika w panelu administratora
                .state('user.adminCreateUser', {
                    url: '/admin/user/create',
                    templateUrl: 'admin_users/create_user_view.html',
                    controller: 'createUserController',
                    controllerAs: 'vm',
                    data : { pageTitle : "Create user" },
                    authorizeRoles: ['ADMIN']
                });

                //Przejscie na sronę główną w przypadku gdy podany adres url nie pasuje do żadnego ze zdefiniowanych w $stateProvider
                $urlRouterProvider.otherwise('/');
        }

        function run($rootScope, $state, sessionSrv){

            $rootScope.$on('$stateChangeStart',
                function(event, toState, toParams, fromState, fromParams, options){
                    // if(toState.authorizeRoles) {
                    //     if((toState.authorizeRoles.indexOf(sessionSrv.role)) < 0){
                    //         event.preventDefault();
                    //     }
                    // }
                });

            $rootScope.$state = $state;
        }
})();