(function() {
    'use strict';
    angular
        .module('app')
        .controller("registerController", registerController);

        registerController.$inject = ['$scope', '$state', 'accountService', '$filter'];

        function registerController($scope, $state, accountService, $filter) {

        var vm = this;

        vm.user = {
            roleList: ['USER'],
            orders: []
        };

        vm.nameRegExr = /(^[A-ZŻŹĆĄŚĘŁÓŃ])+[A-Za-zżźćńółęąś,.'-]+$/;
        vm.loginRegExr = /^[A-Za-z0-9]+$/;
        vm.phoneRegExr = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;

        $scope.$watch('birthDay', function (newValue) {
            console.log('lol')
            vm.user.birthDay = $filter('date')(newValue, 'yyyy-MM-dd');
        });

        vm.register = function() {
            accountService.register(vm.user,
                 function() {
                    $state.go("main.login");
                 },
                function() {
                    vm.registerError = true;
                    vm.errorMessage = "Podczas rejestracji wystąpił błąd, spróbuj ponownie";
                });
        };
    }
})();