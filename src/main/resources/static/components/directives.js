(function() {
    'use strict';

    angular
        .module('directives', [
            'accountResource'
        ])
        .directive("loginValidation", ['accountService', function (accountService) {
            // requires an isloated model
            return {
                // restrict to an attribute type.
                restrict: 'A',
                // element must have ng-model attribute.
                require: 'ngModel',
                link: function (scope, ele, attrs, ctrl) {
                    var valid = {};
                    // add a parser that will process each time the value is
                    // parsed into the model when the user updates it.
                    ctrl.$parsers.unshift(function (value) {
                        if (value) {
                            // test and set the validity after update.
                            accountService.userExist(value, function () {
                                    var valid = true;
                                    ctrl.$setValidity('login', valid);
                                },
                                function () {
                                    var valid = false;
                                    ctrl.$setValidity('login', valid);
                                });
                        }
                        return valid ? value : undefined;
                    });

                }
            }
        }])
        .directive('passconfValidation', function() {
            return {
                require: 'ngModel',
                scope: {
                    reference: '=passconfValidation'
                },
                link: function(scope, elm, attrs, ctrl) {
                    ctrl.$parsers.unshift(function(value) {
                        if (value) {
                            var valid = value == scope.reference;
                            ctrl.$setValidity('password', valid);
                        }
                        return valid ? valid : !valid;
                    });

                    scope.$watch("reference", function(value) {
                        ctrl.$setValidity('password', value === ctrl.$viewValue);
                    });
                }
            }
        })
        .directive("map", function () {
            return {
                restrict: 'E',
                template: '<div></div>',
                replace: true,
                link: function (scope, element, attrs) {
                    var map = new google.maps.Map(document.getElementById(attrs.id), {
                        center: {lat: 50.868, lng: 20.608},
                        zoom: 14,
                        styles: [
                            {
                                elementType: "geometry",
                                stylers: [
                                    {
                                        color: "#212121"
                                    }
                                ]
                            },
                            {
                                elementType: "labels.icon",
                                stylers: [
                                    {
                                        visibility: "off"
                                    }
                                ]
                            },
                            {
                                elementType: "labels.text.fill",
                                stylers: [
                                    {
                                        color: "#757575"
                                    }
                                ]
                            },
                            {
                                elementType: "labels.text.stroke",
                                stylers: [
                                    {
                                        color: "#212121"
                                    }
                                ]
                            },
                            {
                                featureType: "administrative",
                                elementType: "geometry",
                                stylers: [
                                    {
                                        color: "#757575"
                                    }
                                ]
                            },
                            {
                                featureType: "administrative.country",
                                elementType: "labels.text.fill",
                                stylers: [
                                    {
                                        color: "#9e9e9e"
                                    }
                                ]
                            },
                            {
                                featureType: "administrative.land_parcel",
                                stylers: [
                                    {
                                        visibility: "off"
                                    }
                                ]
                            },
                            {
                                featureType: "administrative.locality",
                                elementType: "labels.text.fill",
                                stylers: [
                                    {
                                        color: "#bdbdbd"
                                    }
                                ]
                            },
                            {
                                featureType: "poi",
                                elementType: "labels.text.fill",
                                stylers: [
                                    {
                                        color: "#757575"
                                    }
                                ]
                            },
                            {
                                featureType: "poi.park",
                                elementType: "geometry",
                                stylers: [
                                    {
                                        color: "#181818"
                                    }
                                ]
                            },
                            {
                                featureType: "poi.park",
                                elementType: "labels.text.fill",
                                stylers: [
                                    {
                                        color: "#616161"
                                    }
                                ]
                            },
                            {
                                featureType: "poi.park",
                                elementType: "labels.text.stroke",
                                stylers: [
                                    {
                                        color: "#1b1b1b"
                                    }
                                ]
                            },
                            {
                                featureType: "road",
                                elementType: "geometry.fill",
                                stylers: [
                                    {
                                        color: "#2c2c2c"
                                    }
                                ]
                            },
                            {
                                featureType: "road",
                                elementType: "labels.text.fill",
                                stylers: [
                                    {
                                        color: "#8a8a8a"
                                    }
                                ]
                            },
                            {
                                featureType: "road.arterial",
                                elementType: "geometry",
                                stylers: [
                                    {
                                        color: "#373737"
                                    }
                                ]
                            },
                            {
                                featureType: "road.highway",
                                elementType: "geometry",
                                stylers: [
                                    {
                                        color: "#3c3c3c"
                                    }
                                ]
                            },
                            {
                                featureType: "road.highway.controlled_access",
                                elementType: "geometry",
                                stylers: [
                                    {
                                        color: "#4e4e4e"
                                    }
                                ]
                            },
                            {
                                featureType: "road.local",
                                elementType: "labels.text.fill",
                                stylers: [
                                    {
                                        color: "#616161"
                                    }
                                ]
                            },
                            {
                                featureType: "transit",
                                elementType: "labels.text.fill",
                                stylers: [
                                    {
                                        color: "#757575"
                                    }
                                ]
                            },
                            {
                                featureType: "water",
                                elementType: "geometry",
                                stylers: [
                                    {
                                        color: "#000000"
                                    }
                                ]
                            },
                            {
                                featureType: "water",
                                elementType: "labels.text.fill",
                                stylers: [
                                    {
                                        color: "#3d3d3d"
                                    }
                                ]
                            }
                        ]
                    });
                },
            }
        })
})();