package pl.group7;

import com.fasterxml.jackson.databind.ObjectMapper;
import pl.group7.model.to.OrderTO;
import pl.group7.model.to.UserTO;

/**
 * Created by wojciechowskid on 08/05/2017.
 */
public class TestingUtility {

    public static String asJsonString(final Object obj) {
        try {
            String s = new ObjectMapper().writeValueAsString(obj);
            return s;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static UserTO getUserFromJson(final String json){
        try{
            return new ObjectMapper().readValue(json, UserTO.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static OrderTO getOrderFromJson(final String json){
        try{
            return new ObjectMapper().readValue(json, OrderTO.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
