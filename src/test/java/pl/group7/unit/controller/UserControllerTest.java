package pl.group7.unit.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.group7.DummyObjects;
import pl.group7.TestingUtility;
import pl.group7.model.entity.User;
import pl.group7.model.to.UserTO;
import pl.group7.service.data.UserService;
import pl.group7.service.transfer.UserTransferService;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by allst on 05.05.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTest {

    @Autowired
    WebApplicationContext webContext;
    @MockBean
    UserTransferService userTransferService;
    @MockBean
    UserService userService;
    MockMvc mockMvc;

    private User user = DummyObjects.getDummyUser();
    private User modifiedUser = DummyObjects.getModifiedUser();
    private UserTO userTO = DummyObjects.getDummyUserTO();
    private UserTO modifiedUserTO = DummyObjects.getModifiedUserTO();

    @Before
    public void setUp() throws Exception {
        List<User> users = new ArrayList<>();
        users.add(modifiedUser);
        users.add(user);
        given(this.userTransferService.getTO(user)).willReturn(userTO);
        given(this.userTransferService.getObject(userTO)).willReturn(user);
        given(this.userTransferService.getTO(modifiedUser)).willReturn(modifiedUserTO);
        given(this.userTransferService.getObject(modifiedUserTO)).willReturn(modifiedUser);
        given(this.userService.findAll()).willReturn(users);
        given(this.userService.save(user)).willReturn(user);
        given(this.userService.removeById(1)).willReturn(true);
        given(this.userService.getById(1)).willReturn(user);
        given(this.userService.getById(2)).willReturn(null);
        given(this.userService.modify(modifiedUser)).willReturn(modifiedUser);
        given(this.userService.findByLogin(user.getLogin())).willReturn(user);
        given(this.userService.findByLogin("AAA")).willReturn(null);

        mockMvc = MockMvcBuilders
                .webAppContextSetup(webContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void shouldAddUser() throws Exception {
        given(this.userTransferService.getObject(any())).willReturn(user);
        given(this.userTransferService.getTO(any())).willReturn(userTO);
        mockMvc.perform(post("/api/user")
                        .content(TestingUtility.asJsonString(userTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .with(csrf().asHeader()))
                .andExpect(status().is(200))
                .andExpect(content().json(TestingUtility.asJsonString(userTO)));
    }

    @Test
    @WithMockUser(username = "admin",authorities = {"SU"})
    public void shouldRemoveUserWithSuAuthority() throws Exception {
       mockMvc.perform(delete("/api/user/1")
                .with(csrf().asHeader()))
                .andExpect(status().is(200));
    }

    @Test
    @WithMockUser(username = "Dominik",authorities = {"USER"})
    public void shouldRemoveUserByHimselfAuthenticated() throws Exception {
        mockMvc.perform(delete("/api/user/1")
                .with(csrf().asHeader()))
                .andExpect(status().is(200));
    }

    @Test
    @WithMockUser(username = "user",authorities = {"USER"})
    public void shouldNotRemoveUser() throws Exception {
        mockMvc.perform(delete("/api/user/1")
                .with(csrf().asHeader()))
                .andExpect(status().is(403));
    }

    @Test
    @WithMockUser(username = "admin",authorities = {"SU"})
    public void shouldNotRemoveNotExistedUser() throws Exception {
        mockMvc.perform(delete("/api/user/2")
                .with(csrf().asHeader()))
                .andExpect(status().is(406));
    }

    @Test
    @WithMockUser(username = "admin",authorities = {"SU"})
    public void shouldModifyUserWithSuAuthority() throws Exception {
        given(this.userTransferService.getObject(any())).willReturn(modifiedUser);
        given(this.userTransferService.getTO(any())).willReturn(modifiedUserTO);
        String modifiedUserToJSON = TestingUtility.asJsonString(modifiedUserTO);
        mockMvc.perform(put("/api/user/")
                .content(modifiedUserToJSON)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf().asHeader()))
                .andExpect(status().is(200))
                .andExpect(content().json(modifiedUserToJSON));
    }

    @Test
    @WithMockUser(username = "Dominik",authorities = {"MAKE_ORDER"})
    public void shouldModifyUserByHimselfAuthenticated() throws Exception {
        given(this.userTransferService.getObject(any())).willReturn(modifiedUser);
        given(this.userTransferService.getTO(any())).willReturn(modifiedUserTO);
        String modifiedUserToJSON = TestingUtility.asJsonString(modifiedUserTO);
        mockMvc.perform(put("/api/user")
                .content(modifiedUserToJSON)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf().asHeader()))
                .andExpect(status().is(200))
                .andExpect(content().json(modifiedUserToJSON));
    }

    @Test
    @WithMockUser(username = "user",authorities = {"USER"})
    public void shouldNotModifyUser() throws Exception {
        given(this.userTransferService.getObject(any())).willReturn(modifiedUser);
        given(this.userTransferService.getTO(any())).willReturn(modifiedUserTO);
        mockMvc.perform(put("/api/user")
                .content(TestingUtility.asJsonString(modifiedUserTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf().asHeader()))
                .andExpect(status().is(403));
    }

    @Test
    @WithMockUser(username = "admin",authorities = {"SU"})
    public void shouldReceiveAllUsersWithSuAuthority() throws Exception {
         mockMvc.perform(get("/api/user")
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf().asHeader()))
                .andExpect(status().is(200));
    }

    @Test
    @WithMockUser(username = "user",authorities = {"USER"})
    public void shouldNotReceiveAllUsers() throws Exception {
        mockMvc.perform(get("/api/user")
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf().asHeader()))
                .andExpect(status().is(403));
    }

    @Test
    @WithMockUser(username = "admin",authorities = {"SU"})
    public void shouldReceiveUserWithSuAuthority() throws Exception {
        mockMvc.perform(get("/api/user/1")
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf().asHeader()))
                .andExpect(status().is(200));
    }

    @Test
    @WithMockUser(username = "user",authorities = {"USER"})
    public void shouldNotReceiveUser() throws Exception {
        mockMvc.perform(get("/api/user/1")
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf().asHeader()))
                .andExpect(status().is(403));
    }

    @Test
    public void shouldReturnOkLoginIsFree() throws Exception{
        mockMvc.perform(get("/api/user/login/AAA")
                .with(csrf().asHeader()))
                .andExpect(status().is(200));
    }

    @Test
    public void shouldNotReturnOkLoginIsTaken() throws Exception{
        mockMvc.perform(get("/api/user/login/Dominik")
                .with(csrf().asHeader()))
                .andExpect(status().is(406));
    }

    @Test
    @WithMockUser(username = "Dominik",authorities = {"MAKE_ORDER"})
    public void shouldReturnUserWithAuth() throws Exception{
        given(this.userTransferService.getObject(any())).willReturn(user);
        given(this.userTransferService.getTO(any())).willReturn(userTO);
        mockMvc.perform(post("/api/user/login")
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf().asHeader()))
                .andExpect(status().is(200))
                .andExpect(content().json(TestingUtility.asJsonString(userTO)));
    }
}
