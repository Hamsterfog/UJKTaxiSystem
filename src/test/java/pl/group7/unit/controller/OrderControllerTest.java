package pl.group7.unit.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.group7.DummyObjects;
import pl.group7.TestingUtility;
import pl.group7.model.entity.Order;
import pl.group7.model.to.OrderTO;
import pl.group7.service.data.OrderService;
import pl.group7.service.transfer.OrderTransferService;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by wojciechowskid on 08/05/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OrderControllerTest {

    @Autowired
    WebApplicationContext webContext;
    @MockBean
    OrderService orderService;
    @MockBean
    OrderTransferService orderTransferService;

    MockMvc mockMvc;

    private Order order = DummyObjects.getDummyOrder();
    private Order modifiedOrder = DummyObjects.getModifiedOrder();
    private OrderTO orderTO = DummyObjects.getDummyOrderTO();
    private OrderTO modifiedOrderTO = DummyObjects.getModifiedOrderTO();

    @Before
    public void setUp() throws Exception {
        given(this.orderService.save(order)).willReturn(order);
        given(this.orderService.removeById(1)).willReturn(true);
        given(this.orderService.getById(1)).willReturn(order);
        given(this.orderService.modify(modifiedOrder)).willReturn(modifiedOrder);
        given(this.orderTransferService.getTO(order)).willReturn(orderTO);
        given(this.orderTransferService.getTO(modifiedOrder)).willReturn(modifiedOrderTO);
        given(this.orderTransferService.getObject(orderTO)).willReturn(order);
        given(this.orderTransferService.getObject(modifiedOrderTO)).willReturn(modifiedOrder);
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser(username = "Dominik",authorities = {"MAKE_ORDER"})
    public void shouldAddOrder() throws Exception {
        given(this.orderTransferService.getObject(any())).willReturn(order);
        given(this.orderTransferService.getTO(any())).willReturn(orderTO);
        mockMvc.perform(post("/api/order")
                .content(TestingUtility.asJsonString(orderTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf().asHeader()))
                .andExpect(status().is(200))
                .andExpect(content().json(TestingUtility.asJsonString(orderTO)));
    }

    @Test
    @WithMockUser(username = "admin",authorities = {"SU"})
    public void shouldRemoveOrderWithSuAuthority() throws Exception {
        mockMvc.perform(delete("/api/order/1")
                .with(csrf().asHeader()))
                .andExpect(status().is(200));
    }

    @Test
    @WithMockUser(username = "Dominik",authorities = {"USER"})
    public void shouldNotRemoveOrderByCreatorAuthenticated() throws Exception {
        mockMvc.perform(delete("/api/order/1")
                .with(csrf().asHeader()))
                .andExpect(status().is(403));
    }

    @Test
    @WithMockUser(username = "user",authorities = {"USER"})
    public void shouldNotRemoveOrderByUser() throws Exception {
        mockMvc.perform(delete("/api/order/1")
                .with(csrf().asHeader()))
                .andExpect(status().is(403));
    }

    @Test
    @WithMockUser(username = "admin", authorities = "SU")
    public void shouldGetOrderByIdWithSuAuthority() throws Exception {
        mockMvc.perform(get("/api/order/1")
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf().asHeader()))
                .andExpect(status().is(200))
                .andExpect(content().json(TestingUtility.asJsonString(order)));

    }

    @Test
    @WithMockUser(username = "user", authorities = "MAKE_ORDER")
    public void shouldNotGetOrderByIdWithUser() throws Exception {
        mockMvc.perform(get("/api/order/1")
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf().asHeader()))
                .andExpect(status().is(403));

    }

    @Test
    @WithMockUser(username ="admin", authorities = "SU")
    public void shouldModifyOrderWithProperAuthority() throws  Exception{
        given(this.orderTransferService.getObject(any())).willReturn(modifiedOrder);
        given(this.orderTransferService.getTO(any())).willReturn(modifiedOrderTO);
        mockMvc.perform(put("/api/order")
                .content(TestingUtility.asJsonString(modifiedOrderTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf().asHeader()))
                .andExpect(status().is(200))
                .andExpect(content().json(TestingUtility.asJsonString(modifiedOrderTO)));
    }

    @Test
    @WithMockUser(username ="user", authorities = "MAKE_ORDER")
    public void shouldNotModifyOrderWithCommonUser() throws  Exception{
        given(this.orderTransferService.getObject(any())).willReturn(modifiedOrder);
        given(this.orderTransferService.getTO(any())).willReturn(modifiedOrderTO);
        mockMvc.perform(put("/api/order")
                .content(TestingUtility.asJsonString(modifiedOrderTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .with(csrf().asHeader()))
                .andExpect(status().is(403));
   }

}
