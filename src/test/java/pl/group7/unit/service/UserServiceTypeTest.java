package pl.group7.unit.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import pl.group7.DummyObjects;
import pl.group7.model.entity.User;
import pl.group7.repository.UserRepository;
import pl.group7.service.data.UserService;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;

/**
 * Created by allst on 06.05.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTypeTest {

    @MockBean(UserRepository.class)
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    private User user;

    @Before
    public void startUp(){
        user = DummyObjects.getDummyUser();
    }

    @Test
    public void shouldFindByLogin(){
        given(this.userRepository.findByLogin(user.getLogin())).willReturn(user);
        assertEquals(user,userService.findByLogin(user.getLogin()));
    }
    @Test
    public void shouldNotFindByLogin() {
        given(this.userRepository.findByLogin("Dominik1")).willReturn(user);
        assertEquals(null,userService.findByLogin(user.getLogin()));
    }
    @Test
    public void shouldPersistUser(){
        given(this.userRepository.save(user)).willReturn(user);
        given(this.userRepository.findOne(1L)).willReturn(null);
        given(this.userRepository.findByLogin(user.getLogin())).willReturn(null);
        assertEquals(user,userService.save(user));
    }

    @Test(expected = EntityExistsException.class)
    public void shouldNotPersistUser() {
        given(this.userRepository.findOne(1L)).willReturn(user);
        given(this.userRepository.findByLogin(user.getLogin())).willReturn(user);
        given(this.userRepository.save(user)).willReturn(user);
        userService.save(user);
    }

    @Test
    public void shouldRemoveUser() {
        given(this.userRepository.findOne(1L)).willReturn(user);
        userService.removeById(1L);
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldNotRemoveUser() throws Exception {
        given(this.userRepository.findOne(1L)).willReturn(null);
        userService.removeById(1L);
    }

    @Test
    public void shouldModifyUser() {
        User modifiedUser = DummyObjects.getModifiedUser();
        given(this.userRepository.findOne(1L)).willReturn(user);
        given(this.userRepository.findByLogin(user.getLogin())).willReturn(user);
        given(this.userRepository.save(modifiedUser)).willReturn(modifiedUser);
        assertEquals(modifiedUser,userService.modify(modifiedUser));
    }

    @Test(expected = EntityNotFoundException.class)
    public void shouldNotModifyUser() throws Exception {
        User modifiedUser = DummyObjects.getModifiedUser();
        given(this.userRepository.findOne(1L)).willReturn(null);
        given(this.userRepository.findByLogin(modifiedUser.getLogin())).willReturn(null);
        userService.modify(modifiedUser);
    }

    @Test
    public void shouldFindUserById() {
        given(this.userRepository.findOne(1L)).willReturn(user);
        assertEquals(user,userService.getById(1L));
    }

    @Test
    public void shouldNotFindUserById() throws Exception {
        given(this.userRepository.findOne(1L)).willReturn(null);
        assertEquals(null,userService.getById(1L));
    }

    @Test
    public void shouldReturnAllUsers() {
        List<User> users = new ArrayList<>();
        users.add(DummyObjects.getDummyUser());
        users.add(DummyObjects.getAnotherUser());
        given(this.userRepository.findAll()).willReturn(users);
        assertEquals(users,userService.findAll());
    }

}