package pl.group7.integration.mail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.MailSendException;
import org.springframework.test.context.junit4.SpringRunner;
import pl.group7.service.messaging.MailContentHelper;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by allst on 18.05.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MailContentHelperTest {

    @Autowired
    MailContentHelper mailContentHelper;

    @Test
    public void shouldSendEmailOnDesiredAddress(){
        Map<String,String> map = new HashMap<>();
        map.put("orderNumber", "11123123");
        map.put("userMail", "doominikwoojciechowski@gmail.com");
        mailContentHelper.sendOrderMade(map);
    }

    @Test(expected = MailSendException.class)
    public void shouldNotSendEmailDueToIncorrectEMail(){
        Map<String,String> map = new HashMap<>();
        map.put("orderNumber", "11123123");
        map.put("userMail", "doominikwoojciechowskiiiiigmail.com");
        mailContentHelper.sendOrderMade(map);
    }

}
